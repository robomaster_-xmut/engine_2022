#include "remote.h"
#include "bsp_can.h"
#include "pid.h"
#include "tim.h"
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "math.h"

uint8_t key_W,key_S,key_A,key_D,key_SHIFT,key_CTRL,key_Q,key_E,key_R,key_F,key_G,key_Z,key_X,key_C,key_V;
float remote_speed1=0,remote_speed2=0,remote_speed3=0;    //电机四个车轮速度值（以最大值百分比表示）
uint8_t flag_1=0,flag_2=1;
extern float ch_CAN1[7];
extern moto_info_t motor_info[MOTOR_MAX_NUM];
extern moto_info_t motor_info1[MOTOR_MAX_NUM];
extern pid_struct_t motor_pid_CAN1[7];



extern uint16_t ring;
extern uint16_t ring1;
int32_t ting=2000; //底盘初始速度
int32_t s = 0;
int32_t shang = 0;
int32_t err = 0;
int32_t err1 = 0;
int32_t come_back = 0;
int32_t UP_DOWN = 0;
uint8_t	no_target=0;
uint8_t	have_goal=0;
uint8_t Goal_State=1;
uint8_t SPEED_=1;
uint8_t biao_S=1;
uint8_t biao_C=1;
uint8_t biao_L=0;
uint8_t biao_L1=0;
uint8_t biao_x1=0;
uint8_t biao_R=1;
uint8_t biao_R1=1;
uint8_t visual=1;

int16_t Q_speed=0;
int16_t E_speed=0;
int16_t Help_speed_R=0;
int16_t Help_speed_F=0;
int MODE=0;
uint16_t Help_speed=400;
uint16_t j=0;
int _3508_Speedmax=8000;
int16_t PICH_err=0;
int16_t YAW_err=0;
int16_t PICH_err1=0;
int16_t YAW_err1=0;
int16_t PICH=180;
int16_t YAW=180;
int16_t platform_Q=175;
int16_t HELP=180;
int16_t platform_H=175;
int Card=0;
int Maganize=0;
int Goal_State11=0;
uint32_t  Latest_Remote_Control_Pack_Time = 0;
uint32_t  LED_Flash_Timer_remote_control = 0;
uint32_t speed_time=0,mouse_time=0;
extern uint16_t remled_cnt;
extern uint16_t Eye_pwmval;


int16_t W_speed=0;
int16_t A_speed=0;
int16_t S_speed=0;
int16_t D_speed=0;
int16_t Maxspd=1;
Bug_state_t Bug_state={Power_Disable,Power_Disable,Base_down,Card_in,Loosen,clawcam,clawcam,0};
Bug_ctrl_t Bug_ctrl={0,0,0,0,0,0,Base_down,Card_in,Mine_stop};
Bug_client_t Bug_client={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
RC_Type remote_control;
uint8_t eye_cnt=0;
extern float CLAW_ANGLE,PITCH_ANGLE;

void state_check(void)
{
  uint8_t cabe,caaf,baup,badn;
  cabe=HAL_GPIO_ReadPin(Card_before_GPIO_Port,Card_before_Pin);
  caaf=HAL_GPIO_ReadPin(Card_after_GPIO_Port,Card_after_Pin);
  baup=HAL_GPIO_ReadPin(Up_GPIO_Port,Up_Pin);
  badn=HAL_GPIO_ReadPin(Down_GPIO_Port,Down_Pin);
  
  if(cabe==0&&caaf==1) Bug_state.Card_pos=Card_out;
  else if(cabe==1&&caaf==0) Bug_state.Base_pos=Card_in;
  else Bug_state.Card_pos=Card_middle;
  
  if(baup==0&&badn==1) Bug_state.Base_pos=Base_up;
  else if(baup==1&&badn==0) Bug_state.Base_pos=Base_down;
  else Bug_state.Base_pos=Base_middle;
  
}

void Keymos_control(void)
{
  Bug_client.W=(remote_control.keyBoard.key_code>>0)&0x0001;
  Bug_client.S=(remote_control.keyBoard.key_code>>1)&0x0001;
  Bug_client.A=(remote_control.keyBoard.key_code>>2)&0x0001;
  Bug_client.D=(remote_control.keyBoard.key_code>>3)&0x0001;
  Bug_client.SHIFT=(remote_control.keyBoard.key_code>>4)&0x0001;
  Bug_client.CTRL= (remote_control.keyBoard.key_code>>5)&0x0001;
  Bug_client.Q=(remote_control.keyBoard.key_code>>6)&0x0001;
  Bug_client.E=(remote_control.keyBoard.key_code>>7)&0x0001;
  Bug_client.R=(remote_control.keyBoard.key_code>>8)&0x0001;
  Bug_client.F=(remote_control.keyBoard.key_code>>9)&0x0001;
  Bug_client.G=(remote_control.keyBoard.key_code>>10)&0x0001;
  Bug_client.Z=(remote_control.keyBoard.key_code>>11)&0x0001;
  Bug_client.X=(remote_control.keyBoard.key_code>>12)&0x0001;
  Bug_client.C=(remote_control.keyBoard.key_code>>13)&0x0001;
  Bug_client.V=(remote_control.keyBoard.key_code>>14)&0x0001;
  

  
  if(Latest_Remote_Control_Pack_Time - speed_time >100){
		speed_time = Latest_Remote_Control_Pack_Time;               /**************************************************************************************************************************/
   Bug_client.cli_mode=climode_check();
    if(Bug_client.G==1){                       //视角切换  G
      eye_cnt++;
      if(eye_cnt>2) eye_cnt=0;
      switch(eye_cnt){
        case 0:
          Eye_pwmval=534;
          break;
        case 1:
          Bug_state.Rpi_cam=helpcam;
          Eye_pwmval=477;
          break;
        case 2:
          Bug_state.Rpi_cam=clawcam;
          Eye_pwmval=477;
          break;
     }
   }                                                        /**************************************************************************************************************************/
    if(Bug_client.W) {                                              //wasd解算
      W_speed+=10;if(W_speed>100)W_speed=100;
    } else{
      W_speed-=30;if(W_speed<0)W_speed=0;
    }
    if(Bug_client.S){
      S_speed+=10;if(S_speed>100)S_speed=100;
    }else {
      S_speed-=30;if(S_speed<0)S_speed=0;
    }
    if(Bug_client.A){
      A_speed+=10;if(A_speed>100)A_speed=100;
    }else {
      A_speed-=30;if(A_speed<0)A_speed=0;
    }
    if(Bug_client.D){
      D_speed+=10;if(D_speed>100)D_speed=100;
    }else {
      D_speed-=30;if(D_speed<0)D_speed=0;
    }
    if(Bug_client.SHIFT&Bug_client.E){                           //shift+q减速 e加速
    Maxspd+=2; 
    if(Maxspd>60) Maxspd=60;
    }
    else if(Bug_client.SHIFT&Bug_client.Q){
      Maxspd-=2;
      if(Maxspd<10)  Maxspd=10;
    }
    
    if(Bug_client.cli_mode==normal_mode){                 /***************OK***********************************************************************************************************/
      Eye_pwmval=534;
      Bug_ctrl.Vx=(D_speed-A_speed)*Maxspd;                         //主
      Bug_ctrl.Vy=(W_speed-S_speed)*Maxspd;
      Bug_ctrl.Vz=remote_control.mouse.x*Maxspd;
      Bug_ctrl.Mine_move=Mine_stop;

      if(Bug_client.R) Bug_ctrl.Vup=300;
      else if(Bug_client.F) Bug_ctrl.Vup=-300;
      else Bug_ctrl.Vup=0;
      if(Bug_client.E&&Bug_client.SHIFT==0) Bug_ctrl.Vcard=-1000;
      else if(Bug_client.Q&&Bug_client.SHIFT==0) Bug_ctrl.Vcard=1000;
      else Bug_ctrl.Vcard=0;
    }
    else if(Bug_client.cli_mode==help_mode){               /*****************OK*********************************************************************************************************/
      Bug_state.Rpi_cam=helpcam;
      Eye_pwmval=477;
      Bug_ctrl.Vx=-(D_speed-A_speed)*Maxspd;                         //救援
      Bug_ctrl.Vy=-(W_speed-S_speed)*Maxspd;
      Bug_ctrl.Vz=-remote_control.mouse.x*Maxspd;
      Bug_ctrl.Mine_move=Mine_stop;
      if(Bug_client.R){
        HAL_GPIO_TogglePin(POWER_2_GPIO_Port,POWER_2_Pin);
      }

      if(Bug_client.E&&Bug_client.SHIFT==0) Bug_ctrl.Vcard=-1000;
      else if(Bug_client.Q&&Bug_client.SHIFT==0) Bug_ctrl.Vcard=1000;
      else Bug_ctrl.Vcard=0;
    }
    else if(Bug_client.cli_mode==Goldmine_mode){               /******************************************** ******************************************************************************/
                                              
      Bug_state.Rpi_cam=clawcam;
      Eye_pwmval=477;                                         //金矿ok
      Bug_ctrl.Vx=(W_speed-S_speed)*Maxspd;                     
      Bug_ctrl.Vy=-(D_speed-A_speed)*Maxspd;
      Bug_ctrl.Vz=remote_control.mouse.x*Maxspd;
      Bug_ctrl.Mine_move=Mine_up;
      HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_RESET);//高
      
      if(Bug_client.R) Bug_ctrl.Vup=300;
      else if(Bug_client.F) Bug_ctrl.Vup=-300;
      else Bug_ctrl.Vup=0;
      if(Bug_client.E) Bug_ctrl.Vturn=300;
      else if(Bug_client.Q) Bug_ctrl.Vturn=-300;
      else Bug_ctrl.Vturn=0;
      Bug_ctrl.Vpitch=remote_control.ch2*Vpitchmax/660;
      
      if(Bug_client.V) Bug_client.action.Do=0;                    //金矿
      if(Bug_client.action.Do){
        if(remote_control.mouse.press_left){Bug_client.action.This_over=0;}
        if(!Bug_client.action.This_over){
          switch(Bug_client.action.cnt){
            case 2:                                    //动作一
              Bug_ctrl.Vturn=200;
              if(CLAW_ANGLE>200) {//结束判断
                Bug_ctrl.Vturn=0;
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;}
              break;
                
            case 1:
               
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);
                osDelay(1800);
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);
                osDelay(500);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            
            case 3:                                    //动作三
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET);//夹
                osDelay(800);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 4:
                Bug_ctrl.Vturn=-200;
                if(CLAW_ANGLE<100) {//结束判断
                  HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);
                  Bug_ctrl.Vturn=0;
                  Bug_client.action.cnt++;
                  Bug_client.action.This_over=1;}
                break;
          }
        }
      }
    }
    else if(Bug_client.cli_mode==Skymine_mode){               /**************************************************************************************************************************/
      Bug_state.Rpi_cam=clawcam;
      Eye_pwmval=477;                                         //空接矿
      Bug_ctrl.Vx=(W_speed-S_speed)*Maxspd;                     
      Bug_ctrl.Vy=-(D_speed-A_speed)*Maxspd;
      Bug_ctrl.Vz=remote_control.mouse.x*Maxspd;
      Bug_ctrl.Mine_move=Mine_up;
      HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_RESET);
      
      if(Bug_client.R) Bug_ctrl.Vup=300;
      else if(Bug_client.F) Bug_ctrl.Vup=-300;
      else Bug_ctrl.Vup=0;
      if(Bug_client.E) Bug_ctrl.Vturn=300;
      else if(Bug_client.Q) Bug_ctrl.Vturn=-300;
      else Bug_ctrl.Vturn=0;
      Bug_ctrl.Vpitch=remote_control.ch2*Vpitchmax/660;
      
      if(Bug_client.V) Bug_client.action.Do=0;                    //空接矿
      if(Bug_client.action.Do){
        if(remote_control.mouse.press_left){Bug_client.action.This_over=0;}
        if(!Bug_client.action.This_over){
          switch(Bug_client.action.cnt){
            case 1:                                    //动作一
              HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);
                osDelay(1800);
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);
                osDelay(500);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
                
            case 2:
                Bug_ctrl.Vturn=200;
              if(CLAW_ANGLE>200) {//结束判断
                Bug_ctrl.Vturn=0;
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;}
              break;
            
            case 3:                                    //动作三   空接等待
                if(Bug_state.Is_mine==Mine_is||remote_control.mouse.press_left==1){
                  HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET); 
                  Bug_client.action.cnt++;
                  Bug_client.action.This_over=1;
                  break;
                }
                
            case 4:
                Bug_ctrl.Vturn=-200;
                if(CLAW_ANGLE<100) {//结束判断
                  Bug_ctrl.Vturn=0;
                  Bug_client.action.cnt++;
                  Bug_client.action.This_over=1;}
                break;
            case 5:                                    //动作4
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);//后
                osDelay(1300);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
          }
        }
      }
    }
    else if(Bug_client.cli_mode==Grdmine_mode){               /**************************************************************************************************************************/
      Bug_state.Rpi_cam=clawcam;
      Eye_pwmval=477;                                         //地面矿ok
      Bug_ctrl.Vx=(W_speed-S_speed)*Maxspd;                     
      Bug_ctrl.Vy=-(D_speed-A_speed)*Maxspd;
      Bug_ctrl.Vz=remote_control.mouse.x*Maxspd;
      Bug_ctrl.Mine_move=Mine_up;
      HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_SET);//下
      
      if(Bug_client.R) Bug_ctrl.Vup=300;
      else if(Bug_client.F) Bug_ctrl.Vup=-300;
      else Bug_ctrl.Vup=0;
      if(Bug_client.E) Bug_ctrl.Vturn=300;
      else if(Bug_client.Q) Bug_ctrl.Vturn=-300;
      else Bug_ctrl.Vturn=0;
      Bug_ctrl.Vpitch=remote_control.ch2*Vpitchmax/660;
      
      if(Bug_client.V) Bug_client.action.Do=0;                    //地面矿
      if(Bug_client.action.Do){
        if(remote_control.mouse.press_left){Bug_client.action.This_over=0;}
        if(!Bug_client.action.This_over){
          switch(Bug_client.action.cnt){
            case 2:                                    //动作一

              Bug_ctrl.Vturn=200;
              if(CLAW_ANGLE>150) {//结束判断
                Bug_ctrl.Vturn=0;
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;}
              break;
                
            case 1:
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);//前
                osDelay(1800);
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);//开
                osDelay(500);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            
            case 3:                                    //动作三
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET);//关
                osDelay(500);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 5:
//               Bug_ctrl.claw_limit=100;
                Bug_ctrl.Vturn=-200;
                if(CLAW_ANGLE<100) {//结束判断
                  Bug_ctrl.Vturn=0;
                  Bug_client.action.cnt++;
                  Bug_client.action.This_over=1;}
                break;
             case 4:                                    
                HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_RESET);//起
                osDelay(1500);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 6:                                    
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);//回
                osDelay(300);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
          }
        }
      }
    }
    else if(Bug_client.cli_mode==Exchange_mode){               /***兑换ok***********************************************************************************************************************/
      Bug_state.Rpi_cam=clawcam;
      Eye_pwmval=477;
      HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_RESET);
      Bug_ctrl.Vx=(W_speed-S_speed)*Maxspd;                     
      Bug_ctrl.Vy=-(D_speed-A_speed)*Maxspd;
      Bug_ctrl.Vz=remote_control.mouse.x*Maxspd;
      
      if(Bug_client.R) Bug_ctrl.Vup=300;
      else if(Bug_client.F) Bug_ctrl.Vup=-300;
      else Bug_ctrl.Vup=0;
      if(Bug_client.E) Bug_ctrl.Vturn=300;
      else if(Bug_client.Q) Bug_ctrl.Vturn=-300;
      else Bug_ctrl.Vturn=0;
      Bug_ctrl.Vpitch=remote_control.ch2*Vpitchmax/660;
      
      if(Bug_client.V) {
        Bug_client.action.Do=0;
        HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);//开
      }                  
      if(Bug_client.action.Do){
        if(remote_control.mouse.press_left){Bug_client.action.This_over=0;}
        if(!Bug_client.action.This_over){
          switch(Bug_client.action.cnt){
            case 1:                                    //动作一
              HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET);//关
              Bug_ctrl.Vup=400;
              if(Bug_state.Base_pos==Base_up) {//结束判断
                Bug_ctrl.Vup=0;
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;}
              break;
                
            case 2:
                Bug_ctrl.Vturn=200;
              if(CLAW_ANGLE>120) {//结束判断
                Bug_ctrl.Vturn=0;
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;}
                break;
            
            case 3:
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);//前
                osDelay(800);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 4:                                    //动作三
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);//开
                osDelay(300);
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 5:
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);//回
                osDelay(1800);
                HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET);//关
                osDelay(800);
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);//前
                Bug_client.action.cnt++;
                Bug_client.action.This_over=1;
                break;
            case 6:                                    //动作4
                HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);//回
                Bug_ctrl.Vturn=-200;
                if(CLAW_ANGLE<100) {//结束判断
                  Bug_ctrl.Vturn=0;
                  Bug_client.action.cnt++;
                  Bug_client.action.This_over=1;}
                break;
          }
        }
      }
    }
  }
}

uint8_t climode_check(void)
{
  if(Bug_client.SHIFT&Bug_client.Z) {Bug_client.action.Do=0;Bug_client.action.cnt=1;Bug_client.action.This_over=1; return help_mode;}
  else if(Bug_client.CTRL&Bug_client.Z) {Bug_client.action.Do=0;Bug_client.action.cnt=1;Bug_client.action.This_over=1; return normal_mode;}
  else if(Bug_client.SHIFT&Bug_client.X) {Bug_client.action.Do=1;Bug_client.action.cnt=1;Bug_client.action.This_over=0; return Goldmine_mode;}
  else if(Bug_client.CTRL&Bug_client.X) {Bug_client.action.Do=1;Bug_client.action.cnt=1;Bug_client.action.This_over=0;return Skymine_mode;}
  else if(Bug_client.SHIFT&Bug_client.C) {Bug_client.action.Do=1;Bug_client.action.cnt=1;Bug_client.action.This_over=0;return Grdmine_mode;}
  else if(Bug_client.CTRL&Bug_client.C) {Bug_client.action.Do=1;Bug_client.action.cnt=1;Bug_client.action.This_over=0;return Exchange_mode;}
  else return Bug_client.cli_mode;
}

void Callback_RC_Handle(RC_Type* rc, uint8_t* buff)
{

	rc->ch1 = (buff[0] | buff[1]<<8) & 0x07FF;
	rc->ch1 -= 1024;
	rc->ch2 = (buff[1]>>3 | buff[2]<<5 ) & 0x07FF;
	rc->ch2 -= 1024;
	rc->ch3 = (buff[2]>>6 | buff[3]<<2 | buff[4]<<10) & 0x07FF;
	rc->ch3 -= 1024;
	rc->ch4 = (buff[4]>>1 | buff[5]<<7) & 0x07FF;		
	rc->ch4 -= 1024;
	
	rc->switch_left = ( (buff[5] >> 4)& 0x000C ) >> 2;
	rc->switch_right =  (buff[5] >> 4)& 0x0003 ;
	
	rc->sw = (buff[16] | (buff[17] << 8)) & 0x07FF;
	rc->sw -= 1024;
	
	rc->mouse.x = buff[6] | (buff[7] << 8);	//x axis
	rc->mouse.y = buff[8] | (buff[9] << 8);
	rc->mouse.z = buff[10]| (buff[11] << 8);
	
	rc->mouse.press_left 	= buff[12];	// is pressed?
	rc->mouse.press_right = buff[13];
	
	rc->keyBoard.key_code = buff[14] | buff[15] << 8; //key borad code
	
	remled_cnt++;
  if(remled_cnt==45000){
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin);
    remled_cnt=0;
  }
}
int i;
void RC_control (void)    //控制算法，分模式控制
{ 
	Latest_Remote_Control_Pack_Time = HAL_GetTick();
	
	if(remote_control.switch_right==0x01&&remote_control.switch_left==0x01)MODE=1;
  else if(remote_control.switch_right==0x01&&remote_control.switch_left==0x03)MODE=2;
  else if(remote_control.switch_right==0x03&&remote_control.switch_left==0x01)MODE=3;
	else MODE=0;
   	/****************数据处理***************/
	
	/*******模式0解析*****/// l:2 r:1 
	if(MODE==0){        
		/*右前轮*/ch_CAN1[0]= -remote_control.ch4*8000/660+remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*右后轮*/ch_CAN1[1]= -remote_control.ch4*8000/660-remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*左后轮*/ch_CAN1[2]= remote_control.ch4*8000/660-remote_control.ch3*8000/660+remote_control.ch1*8000/660;
		/*左前轮*/ch_CAN1[3]= remote_control.ch4*8000/660+remote_control.ch3*8000/660+remote_control.ch1*8000/660;
			ch_CAN1[4]=remote_control.ch2*2000/660;
			ch_CAN2[0]=0;ch_CAN2[1]=0;ch_CAN2[2]=0;ch_CAN2[3]=0;
			if(ch_CAN1[0]>_3508_Speedmax) ch_CAN1[0]=_3508_Speedmax;  if(ch_CAN1[0]<-_3508_Speedmax) ch_CAN1[0]=-_3508_Speedmax;
			if(ch_CAN1[1]>_3508_Speedmax) ch_CAN1[1]=_3508_Speedmax;  if(ch_CAN1[1]<-_3508_Speedmax) ch_CAN1[1]=-_3508_Speedmax;
			if(ch_CAN1[2]>_3508_Speedmax) ch_CAN1[2]=_3508_Speedmax;  if(ch_CAN1[2]<-_3508_Speedmax) ch_CAN1[2]=-_3508_Speedmax;
			if(ch_CAN1[3]>_3508_Speedmax) ch_CAN1[3]=_3508_Speedmax;  if(ch_CAN1[3]<-_3508_Speedmax) ch_CAN1[3]=-_3508_Speedmax;
	}
	
	/*******模式1解析*****/// l:1 r:1
	if(MODE==1){        //  1 * 1             use：ch3、ch4
		 ch_CAN1[0]=0;ch_CAN1[1]=0;ch_CAN1[2]=0;ch_CAN1[3]=0;ch_CAN1[4]=0;
			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
			ch_CAN2[1]= remote_control.ch4*2000/660;
			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
			ch_CAN2[3]= remote_control.ch3*2000/660;
    
	 	 PICH=-remote_control.ch2*120/660+180;
		 //__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_1, PICH+50);  //修改pwm占空比
	 	 YAW=-remote_control.ch1*120/660+180;
     //__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_2, YAW);  //修改pwm占空比
    
    

//		if(ring>31){
//			ch_CAN2[0]=0;
//			ch_CAN2[1]=0;
//		}
//		else{
//			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
//			ch_CAN2[1]= remote_control.ch4*2000/660;
//		}
//		if(ring1>9){
//			ch_CAN2[2]=0;
//			ch_CAN2[3]=0;
//		}
//		else{
//			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
//			ch_CAN2[3]= remote_control.ch3*2000/660;
//		}
		if(remote_control.sw<-100){
			
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_RESET);//闭
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_SET);
		}
		if(remote_control.sw<-600){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);
		}
		if(remote_control.sw>100){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_SET);//开
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_RESET);
		}
		if(remote_control.sw>600){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
		}
		 ring=0;
		 ring1=0;
		 biao_L=0;
		 biao_R=1;
	}
	
	
	/*******模式2解析*****/// l:3 r:1
	if(MODE==2){              //  3 * 1
		ch_CAN1[0]=0;ch_CAN1[1]=0;ch_CAN1[2]=0;ch_CAN1[3]=0;ch_CAN1[4]=0;
		ch_CAN2[0]=0;ch_CAN2[1]=0;ch_CAN2[2]=0;ch_CAN2[3]=0;
	 	 YAW=-remote_control.ch2*120/660+175;
	 	 PICH=+remote_control.ch2*120/660+175;
//     __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, PICH);  //修改pwm占空比
//     __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, YAW);  //修改pwm占空比
     if(remote_control.ch1>600){Maganize=200;Card=100;}
     if(remote_control.ch1<-600){Maganize=140;Card=160;}
//     __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_4, Card);  //修改pwm占空比
//     __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, Maganize);  //修改pwm占空比
		 if(remote_control.sw>500)
			 HELP=50;
		 else
			 HELP=180;
//		 __HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, HELP);  //修改pwm占空比
		 ring=0;
		 ring1=0;
		 biao_L=0;
		 biao_R=1;
	}
	
	
	/*******模式3解析*****/// l:1 r:3
	if(MODE==3){                // 1 * 3
		/*******键盘解析*****/
		if(Latest_Remote_Control_Pack_Time - speed_time >100){
			speed_time = Latest_Remote_Control_Pack_Time;
	//		key_W 
			if((remote_control.keyBoard.key_code>>0)&0x0001){
				W_speed+=10;if(W_speed>100)W_speed=100;
			}else {
				W_speed-=30;if(W_speed<0)W_speed=0;
			}
	//		key_S 
			if((remote_control.keyBoard.key_code>>1)&0x0001){
				S_speed+=10;if(S_speed>100)S_speed=100;
			}else {
				S_speed-=30;if(S_speed<0)S_speed=0;
			}
	//		key_A 
			if((remote_control.keyBoard.key_code>>2)&0x0001){
				A_speed+=10;if(A_speed>100)A_speed=100;
			}else {
				A_speed-=30;if(A_speed<0)A_speed=0;
			}
	//		key_D 
			if((remote_control.keyBoard.key_code>>3)&0x0001){
				D_speed+=10;if(D_speed>100)D_speed=100;
			}else {
				D_speed-=30;if(D_speed<0)D_speed=0;
			}
	//		key_Q 
			if((remote_control.keyBoard.key_code>>6)&0x0001){
				Q_speed+=10;if(Q_speed>100)Q_speed=100;
			}else {
				Q_speed-=30;if(Q_speed<0)Q_speed=0;
			}
	//		key_E 
			if((remote_control.keyBoard.key_code>>7)&0x0001){
				E_speed+=10;if(E_speed>100)E_speed=100;
			}else {
				E_speed-=30;if (E_speed<0)E_speed=0;
			}
		}
		key_SHIFT = (remote_control.keyBoard.key_code>>4)&0x0001;
		if((key_SHIFT)&&biao_S){SPEED_++;SPEED_%=6;biao_S=0;}
		else if(!key_SHIFT)biao_S=1;
		key_CTRL = (remote_control.keyBoard.key_code>>5)&0x0001;
		if((key_CTRL)&&biao_C){SPEED_--;if(SPEED_<1)SPEED_=1;biao_C=0;}
		else if(!key_CTRL)biao_C=1;
		
//		key_R 
		if((remote_control.keyBoard.key_code>>8)&0x0001)Help_speed_R=400;else Help_speed_R=0;
//		key_F 
 		if((remote_control.keyBoard.key_code>>9)&0x0001)Help_speed_F=-400;else Help_speed_F=0;
//		key_G 
    if(((remote_control.keyBoard.key_code>>10)&0x0001)&&Goal_State){
			have_goal++;have_goal%=2;Goal_State=0;
			switch(have_goal){
				case 0:Maganize=200;Card=100;break;
				case 1:Maganize=140;Card=160;break;
			}
		}else if(!((remote_control.keyBoard.key_code>>10)&0x0001))
			Goal_State=1;
//		key_Z 
    if(((remote_control.keyBoard.key_code>>11)&0x0001)&&biao_R){
			biao_L++;biao_L%=3;biao_R=0;biao_R1=0;if(biao_L==0)biao_R=1;ring=0;ring1=0;
		}
		if(biao_L==1){
			if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_5)){ch_CAN2[0]= -1000;ch_CAN2[1]= 1000;
			}else {ch_CAN2[0]=0;ch_CAN2[1]=0;
			}
			if(ring1>=9){
				ch_CAN2[2]=0;ch_CAN2[3]=0;
			}else{
				ch_CAN2[2]= 1000;ch_CAN2[3]= -1000;
				if(motor_info1[2].torque_current>13000){
				ch_CAN2[2]=0;ch_CAN2[3]=0;biao_R1=1;}
			}
			if(ring1>=9){
				biao_R=1;
			}
		}else if(biao_L==2){
			if(ring>=30){
				ch_CAN2[0]=0;ch_CAN2[1]=0;
			}else{
				ch_CAN2[0]= 1000;ch_CAN2[1]= -1000;
			}
			if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_4)){ch_CAN2[2]= -600;ch_CAN2[3]= 600;
			}else {ch_CAN2[2]=0;ch_CAN2[3]=0;
			}
			if(ring>=30){
				biao_R=1;
			}
		}
		
    if(((remote_control.keyBoard.key_code>>12)&0x0001)&&biao_x1){
			flag_1++;if(flag_1>2)flag_1=1;
		}if(remote_control.mouse.press_right)biao_x1=0;else biao_x1=1;
		if(flag_1==1){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_SET);//开
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_RESET);
//			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
//			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
		}else if(flag_1==2){
			HAL_GPIO_WritePin(GPIOH, POWER_3_Pin, GPIO_PIN_RESET);//闭
			HAL_GPIO_WritePin(GPIOH, POWER_4_Pin, GPIO_PIN_SET);
//			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
//			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);
		}
			if(remote_control.mouse.press_left){
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_RESET);//前
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_SET);
			}else 
			{
			HAL_GPIO_WritePin(GPIOH, POWER_1_Pin, GPIO_PIN_SET);//后
			HAL_GPIO_WritePin(GPIOH, POWER_2_Pin, GPIO_PIN_RESET);}
//		key_C =    (remote_control.keyBoard.key_code>>13)&0x0001;
//		key_V     
		if((remote_control.keyBoard.key_code>>14)&0x0001)HELP=50;else HELP=180;
		
		
		/*******底盘控制*****/
		/*右前轮*/ch_CAN1[0]= (-W_speed+S_speed-A_speed+D_speed-Q_speed+E_speed)*SPEED_*10;
		/*右后轮*/ch_CAN1[1]= (-W_speed+S_speed+A_speed-D_speed-Q_speed+E_speed)*SPEED_*10;
		/*左后轮*/ch_CAN1[2]= (W_speed-S_speed+A_speed-D_speed-Q_speed+E_speed)*SPEED_*10;
		/*左前轮*/ch_CAN1[3]= (W_speed-S_speed-A_speed+D_speed-Q_speed+E_speed)*SPEED_*10;
		ch_CAN1[4]=Help_speed_R+Help_speed_F;
		for(uint8_t i=0;i<4;i++)
		{  if(ch_CAN1[i]>6000)ch_CAN1[i]=6000;
			if(ch_CAN1[i]<-6000)ch_CAN1[i]=-6000;
		}
		
		/*******云台控制*****//*******鼠标解析*****/
		if(Latest_Remote_Control_Pack_Time - mouse_time >100){
			mouse_time = Latest_Remote_Control_Pack_Time;
			if(remote_control.mouse.x>10){
				YAW_err-=10;if(YAW_err<-120)YAW_err=-120;
			}
			else if(remote_control.mouse.x<-10){
				YAW_err+=10;if(YAW_err>120)YAW_err=120;
			}
			if(remote_control.mouse.y>10){
				PICH_err-=10;if(PICH_err<-120)PICH_err=-120;
			}
			else if(remote_control.mouse.y<-10){
				PICH_err+=10;if(PICH_err>120)PICH_err=120;
			}
		}
		if((remote_control.mouse.press_right)&&biao_L1){visual++;if(visual>3)visual=1;}//Ctrl键减挡位
			if(remote_control.mouse.press_right)biao_L1=0;else biao_L1=1;
	 if(!biao_L1){
	switch(visual){
		case 1:YAW_err=0;PICH_err=0;break;
		case 2:YAW_err=0;PICH_err=50;break;
		case 3:YAW_err=60;PICH_err=0;break;
	}}
//		if(remote_control.mouse.press_right){YAW_err=0;PICH_err=0;}
		YAW = 180+YAW_err;PICH = 180+PICH_err;
//		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_1, PICH);  //修改pwm占空比
//		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_2, YAW);  //修改pwm占空比
//		__HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_4, Card);  //修改pwm占空比
//    __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, Maganize);  //修改pwm占空比
//		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_2, platform_H);  //修改pwm占空比
//		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_1, platform_Q);  //修改pwm占空比
//		__HAL_TIM_SetCompare(&htim5, TIM_CHANNEL_4, HELP);  //修改pwm占空比
		
//		if(ring>10){
//			ch_CAN2[0]=0;
//			ch_CAN2[1]=0;
//		}
//		else{
//			ch_CAN2[0]= -remote_control.ch4*2000/660;//+
//			ch_CAN2[1]= remote_control.ch4*2000/660;
//		}
//		if(ring1>7){
//			ch_CAN2[2]=0;
//			ch_CAN2[3]=0;
//		}
//		else{
//			ch_CAN2[2]= -remote_control.ch3*2000/660;//-
//			ch_CAN2[3]= remote_control.ch3*2000/660;
//		}
	}
 
	
}





