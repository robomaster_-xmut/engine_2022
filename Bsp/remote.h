#ifndef _REMOTE_H
#define _REMOTE_H

#include "main.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"
typedef struct {
	int16_t ch1;	//each ch value from -364 -- +364
	int16_t ch2;
	int16_t ch3;
	int16_t ch4;
	
	uint8_t switch_left;	//3 value
	uint8_t switch_right;
	
	int16_t sw;
	struct {
		int16_t x;
		int16_t y;
		int16_t z;
	
		uint8_t press_left;
		uint8_t press_right;
	}mouse;
	
	struct {
		uint16_t key_code;
/**********************************************************************************
   * 键盘通道:15   14   13   12   11   10   9   8   7   6     5     4   3   2   1
   *           V    C    X	  Z    G    F   R   E   Q  CTRL  SHIFT  D   A   S   W
************************************************************************************/

	}keyBoard;
	

}RC_Type;

typedef struct {            //标志位结构体
  uint8_t Chassis_enable;
  uint8_t Gimbal_enable;
  uint8_t Base_pos;
  uint8_t Card_pos;
  uint8_t Is_mine;
  uint8_t Rpi_cam;
  uint8_t Last_rpi_cam;
  uint8_t Mine_d;
}Bug_state_t;//

typedef struct {          //控制方向参数，速度，位置
  float Vz;
  float Vx;
  float Vy;
  int16_t Vup;
  int16_t Vturn;
  int16_t Vpitch;
  int16_t Vcard;
  uint8_t Base_target;
  uint8_t Card_target;
  int16_t Mine_move;
  uint16_t  claw_limit;
}Bug_ctrl_t;//={0,0,0,0,Base_down,Card_in,Mine_stop,Mine_stop}

typedef struct {
  uint8_t cli_mode;
  uint8_t W;
  uint8_t A;
  uint8_t S;
  uint8_t D;
  uint8_t SHIFT;
  uint8_t CTRL;
  uint8_t Q;
  uint8_t E;
  uint8_t R;
  uint8_t F;
  uint8_t G;
  uint8_t Z;
  uint8_t X;
  uint8_t C;
  uint8_t V;
  uint8_t task_over;
  uint8_t task_cnt;
  struct{
    uint8_t Do;
    uint8_t Last_over;
    uint8_t This_over;
    uint8_t mos;
    uint8_t cnt;
  }action;
}Bug_client_t;

#define normal_mode   0
#define help_mode     1
#define Goldmine_mode 2
#define Skymine_mode  3
#define Exchange_mode 4
#define Grdmine_mode  5


uint8_t climode_check(void);
void state_check(void);
void RemoteDataPrcess(uint8_t *pData);//数据处理(处理+校验)
void RC_control (void);
void Callback_RC_Handle(RC_Type* rc, uint8_t* buff);
void Turning_Laps(uint16_t num,float speed);
void Keymos_control(void);
#endif  

