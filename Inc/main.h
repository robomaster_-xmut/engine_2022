/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define LIMIT_MIN_MAX(x,min,max) (x) = (((x)<=(min))?(min):(((x)>=(max))?(max):(x)))
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define POWER_1_Pin GPIO_PIN_2
#define POWER_1_GPIO_Port GPIOH
#define POWER_2_Pin GPIO_PIN_3
#define POWER_2_GPIO_Port GPIOH
#define POWER_3_Pin GPIO_PIN_4
#define POWER_3_GPIO_Port GPIOH
#define LED8_Pin GPIO_PIN_8
#define LED8_GPIO_Port GPIOG
#define POWER_4_Pin GPIO_PIN_5
#define POWER_4_GPIO_Port GPIOH
#define LED7_Pin GPIO_PIN_7
#define LED7_GPIO_Port GPIOG
#define LED6_Pin GPIO_PIN_6
#define LED6_GPIO_Port GPIOG
#define LED5_Pin GPIO_PIN_5
#define LED5_GPIO_Port GPIOG
#define LED4_Pin GPIO_PIN_4
#define LED4_GPIO_Port GPIOG
#define LED3_Pin GPIO_PIN_3
#define LED3_GPIO_Port GPIOG
#define LED2_Pin GPIO_PIN_2
#define LED2_GPIO_Port GPIOG
#define Ismine_Pin GPIO_PIN_0
#define Ismine_GPIO_Port GPIOC
#define Ismine_EXTI_IRQn EXTI0_IRQn
#define KEY_Pin GPIO_PIN_2
#define KEY_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_1
#define LED1_GPIO_Port GPIOG
#define Down_Pin GPIO_PIN_13
#define Down_GPIO_Port GPIOD
#define Down_EXTI_IRQn EXTI15_10_IRQn
#define Minedown_Pin GPIO_PIN_1
#define Minedown_GPIO_Port GPIOA
#define Up_Pin GPIO_PIN_12
#define Up_GPIO_Port GPIOD
#define Up_EXTI_IRQn EXTI15_10_IRQn
#define Card_after_Pin GPIO_PIN_2
#define Card_after_GPIO_Port GPIOA
#define Card_after_EXTI_IRQn EXTI2_IRQn
#define LED_RED_Pin GPIO_PIN_11
#define LED_RED_GPIO_Port GPIOE
#define Card_before_Pin GPIO_PIN_3
#define Card_before_GPIO_Port GPIOA
#define Card_before_EXTI_IRQn EXTI3_IRQn
#define LED_GREEN_Pin GPIO_PIN_14
#define LED_GREEN_GPIO_Port GPIOF
/* USER CODE BEGIN Private defines */
#define lose_ctrl 0
#define remote_move_mode 1
#define remote_claw_mode 2
#define remote_mine_mode 3
#define keymos_mode      4

#define clawcam         1
#define helpcam         2

#define Power_Enable    1       //上电标志
#define Power_Disable   0
#define Press           1       //按下标志
#define Loosen          0

#define Base_up         2       //抬升基座位置
#define Base_middle     1
#define Base_down       0

#define Card_out        2       //卡位置
#define Card_middle     1
#define Card_in         0

#define Mine_no         0       //矿石是否存在
#define Mine_is         1

#define Mine_stop       0       //转矿机构大速度
#define Mine_down       1
#define Mine_up         2
#define Mine_left       3
#define Mine_right      4

#define Chassis_speedmax   2500
#define Up_speedmax        600
#define Turn_speedmax      600
#define Vpitchmax          3000
#define Vcardmax           1000
#define Mineturnspd        3000

extern uint8_t UART_Buffer[100];
extern unsigned char UART6_RECV_BUF[128];//串口6 接受 发送缓存
extern float ch_CAN1[7];
extern float ch_CAN2[7];
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
