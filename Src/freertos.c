/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "remote.h"
#include "bsp_usart.h"
#include "fifo.h"
#include "tim.h"
#include "crc8_crc16.h"
#include "rc.h"
extern float CLAW_ANGLE,PITCH_ANGLE;
extern int _3508_Speedmax;
extern unsigned char  RX6_BUFFER[20];
extern unsigned char  RX8_BUFFER[20];
extern UART_HandleTypeDef huart3;
extern uint8_t usart3_buf[2][USART_RX_BUF_LENGHT];
extern fifo_s_t referee_fifo;
extern uint8_t referee_fifo_buf[REFEREE_FIFO_BUF_LENGTH];
extern RC_Type remote_control;
extern uint8_t ctrl_mode;
extern Bug_state_t Bug_state;
extern Bug_ctrl_t Bug_ctrl;
extern ext_game_robot_state_t robot_state;
extern uint16_t Eye_pwmval;
uint16_t color_ID,robot_ID;
extern uint32_t  Latest_Remote_Control_Pack_Time;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
osThreadId myTask04Handle;
osThreadId myTask05Handle;
osThreadId myTask06Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);
void StartTask03(void const * argument);
void StartTask04(void const * argument);
void StartTask05(void const * argument);
void StartTask06(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityIdle, 0, 128);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, StartTask03, osPriorityIdle, 0, 128);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, StartTask04, osPriorityIdle, 0, 128);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* definition and creation of myTask05 */
  osThreadDef(myTask05, StartTask05, osPriorityIdle, 0, 128);
  myTask05Handle = osThreadCreate(osThread(myTask05), NULL);

  /* definition and creation of myTask06 */
  osThreadDef(myTask06, StartTask06, osPriorityIdle, 0, 128);
  myTask06Handle = osThreadCreate(osThread(myTask06), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */                                                                  //模式控制，运行检测，功能使能失能控制线程****************************************************
  for(;;)
  {
    Bug_state.Chassis_enable=robot_state.mains_power_chassis_output;    //功率开关
    Bug_state.Gimbal_enable =robot_state.mains_power_gimbal_output;
    
    //模式选择
    if(remote_control.switch_right==2&&remote_control.switch_left==2)               
      ctrl_mode=keymos_mode;                                                        //  2--2  客户端控制
    else if(remote_control.switch_right==1&&remote_control.switch_left==1)
      ctrl_mode=remote_move_mode;                                                   //  1--1  遥控移动+救援
    else if(remote_control.switch_right==1&&remote_control.switch_left==3)
      ctrl_mode=remote_claw_mode;                                                   //  1--3  遥控抓取+抬升
    else if(remote_control.switch_right==1&&remote_control.switch_left==2)
      ctrl_mode=remote_mine_mode;                                                   //  1--2  遥控转矿
    else 
      ctrl_mode=lose_ctrl;                                                          //  other  其他情况，静止

    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */                                  //数据解算，动作控制，速度设置线程 //键鼠模式下运动控制，动作序列控制算法线程
  for(;;)
  {
    Latest_Remote_Control_Pack_Time = HAL_GetTick();
    if(ctrl_mode == keymos_mode)
    {
      Keymos_control();                                                               //  2--2  客户端控制
    }
    else if (ctrl_mode==remote_move_mode){
      Bug_ctrl.Vx=(float)remote_control.ch3*Chassis_speedmax/660;                     //  1--1  遥控移动+救援
      Bug_ctrl.Vy=(float)remote_control.ch4*Chassis_speedmax/660;
      Bug_ctrl.Vz=(float)remote_control.ch1*Chassis_speedmax/660;
      
      Bug_ctrl.Vup=-(float)remote_control.sw*Up_speedmax/660;
      
      Bug_ctrl.Vcard=-remote_control.ch2*Vcardmax/660;
      Eye_pwmval=534;
    }
    else if (ctrl_mode==remote_claw_mode){
       Bug_ctrl.Vx=0; Bug_ctrl.Vy=0; Bug_ctrl.Vz=0; Bug_ctrl.Vup=0; Bug_ctrl.Vturn=0;                      //  1--3  遥控抓取+抬升

      if(remote_control.ch1>300) HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_SET);
      else if(remote_control.ch1<-300) HAL_GPIO_WritePin(POWER_1_GPIO_Port,POWER_1_Pin,GPIO_PIN_RESET);
      if(remote_control.ch2>300) HAL_GPIO_WritePin(POWER_2_GPIO_Port,POWER_2_Pin,GPIO_PIN_SET);
      else if(remote_control.ch2<-300) HAL_GPIO_WritePin(POWER_2_GPIO_Port,POWER_2_Pin,GPIO_PIN_RESET);
      if(remote_control.ch3>300) HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_SET);
      else if(remote_control.ch3<-300) HAL_GPIO_WritePin(POWER_3_GPIO_Port,POWER_3_Pin,GPIO_PIN_RESET);
      if(remote_control.ch4>300) HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_SET);
      else if(remote_control.ch4<-300) HAL_GPIO_WritePin(POWER_4_GPIO_Port,POWER_4_Pin,GPIO_PIN_RESET);
      Eye_pwmval=477;
    }
    else if (ctrl_mode==remote_mine_mode){                                            //  1--2  遥控转矿
      Bug_ctrl.Vx=0; Bug_ctrl.Vy=0; Bug_ctrl.Vz=0;
      if(remote_control.ch2>300)                               Bug_ctrl.Mine_move=Mine_up;
      else if(remote_control.ch2<-300)                         Bug_ctrl.Mine_move=Mine_down;
      else if(remote_control.ch1>300)                          Bug_ctrl.Mine_move=Mine_right;
      else if(remote_control.ch1<-300)                         Bug_ctrl.Mine_move=Mine_left;
      else if(remote_control.ch1==0 && remote_control.ch2==0)  Bug_ctrl.Mine_move=Mine_stop;
      
      Bug_ctrl.Vup=(float)remote_control.ch4*Up_speedmax/660;
      Bug_ctrl.Vturn=(float)remote_control.ch3*Turn_speedmax/660;
      Bug_ctrl.Vpitch=(float)remote_control.sw*Vpitchmax/660;
      Eye_pwmval=477;
    }
    else if (ctrl_mode==lose_ctrl){ Bug_ctrl.Vx=0; Bug_ctrl.Vy=0; Bug_ctrl.Vz=0; Bug_ctrl.Vup=0;Bug_ctrl.Vturn=0;Bug_ctrl.Vpitch=0;Eye_pwmval=534;Bug_ctrl.Vcard=0;} //其他情况全停
      
      
    if(Bug_state.Base_pos==Base_down&&Bug_ctrl.Vup<0) Bug_ctrl.Vup=0;         //配合微动限制卡和抬升
    if(Bug_state.Base_pos==Base_up&&Bug_ctrl.Vup>0) Bug_ctrl.Vup=0;
    if(Bug_state.Card_pos==Card_out&&Bug_ctrl.Vcard<0) Bug_ctrl.Vcard=0;
    if(Bug_state.Card_pos==Card_in&&Bug_ctrl.Vcard>0) Bug_ctrl.Vcard=0;
    if(Bug_state.Card_pos==Card_in) {Bug_ctrl.Vcard=-3000;osDelay(50);Bug_ctrl.Vcard=0;}
    else if(Bug_state.Card_pos==Card_out) {Bug_ctrl.Vcard=3000;osDelay(50);Bug_ctrl.Vcard=0;}
    osDelay(1);
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void const * argument)
{
  /* USER CODE BEGIN StartTask03 */
  /* Infinite loop */                                                       //数据处理，电机分速度计算
  for(;;)
  {
    /*右前轮*/ch_CAN1[0]= -Bug_ctrl.Vy+Bug_ctrl.Vx+Bug_ctrl.Vz;
		/*右后轮*/ch_CAN1[1]= -Bug_ctrl.Vy-Bug_ctrl.Vx+Bug_ctrl.Vz;
		/*左后轮*/ch_CAN1[2]= Bug_ctrl.Vy-Bug_ctrl.Vx+Bug_ctrl.Vz;
		/*左前轮*/ch_CAN1[3]= Bug_ctrl.Vy+Bug_ctrl.Vx+Bug_ctrl.Vz;
    if(ch_CAN1[0]>_3508_Speedmax) ch_CAN1[0]=_3508_Speedmax;  if(ch_CAN1[0]<-_3508_Speedmax) ch_CAN1[0]=-_3508_Speedmax;
    if(ch_CAN1[1]>_3508_Speedmax) ch_CAN1[1]=_3508_Speedmax;  if(ch_CAN1[1]<-_3508_Speedmax) ch_CAN1[1]=-_3508_Speedmax;
    if(ch_CAN1[2]>_3508_Speedmax) ch_CAN1[2]=_3508_Speedmax;  if(ch_CAN1[2]<-_3508_Speedmax) ch_CAN1[2]=-_3508_Speedmax;
    if(ch_CAN1[3]>_3508_Speedmax) ch_CAN1[3]=_3508_Speedmax;  if(ch_CAN1[3]<-_3508_Speedmax) ch_CAN1[3]=-_3508_Speedmax;
    ch_CAN1[4]=-Bug_ctrl.Vup;
    ch_CAN1[5]= Bug_ctrl.Vup;
    
    ch_CAN2[0]=Bug_ctrl.Vcard;

    ch_CAN2[1]=Bug_ctrl.Vpitch;
    
    ch_CAN2[2]= Bug_ctrl.Vturn;
    ch_CAN2[3]=-Bug_ctrl.Vturn;
    
    switch(Bug_ctrl.Mine_move){
      case Mine_up:
        ch_CAN2[4]=Mineturnspd;ch_CAN2[5]=-Mineturnspd;break;
      case Mine_down:
        ch_CAN2[4]=-Mineturnspd;ch_CAN2[5]=Mineturnspd;break;
      case Mine_right:
        ch_CAN2[4]=Mineturnspd;ch_CAN2[5]=Mineturnspd;break;
      case Mine_left:
        ch_CAN2[4]=-Mineturnspd;ch_CAN2[5]=-Mineturnspd;break;
      case Mine_stop:
        ch_CAN2[4]=0;ch_CAN2[5]=0;break;
    }
    __HAL_TIM_SetCompare(&htim4, TIM_CHANNEL_3, Eye_pwmval);
    if(Bug_state.Rpi_cam!=Bug_state.Last_rpi_cam){
      if(Bug_state.Rpi_cam==1)    HAL_UART_Transmit(&huart7,(uint8_t *)"1",1,0xffff);
      else if(Bug_state.Rpi_cam==2) HAL_UART_Transmit(&huart7,(uint8_t *)"2",1,0xffff);
      Bug_state.Last_rpi_cam=Bug_state.Rpi_cam;
    }
    osDelay(1);
  }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void const * argument)
{
  /* USER CODE BEGIN StartTask04 */
  /* Infinite loop */                                                                     //UI绘制线程***********************************************************************************
  for(;;)
  {
    if(robot_state.robot_id == RED_ENGINEER){
			color_ID = 2;robot_ID = 0x0102;
		}
    else if(robot_state.robot_id == BLUE_ENGINEER){
			color_ID = 102;robot_ID = 0x0166;
		}
    
    ui_draw(0x0101,robot_ID,color_ID,1,1,0,2,0,0,0,2,960,960,0,640,140);//画UI
		ui_draw(0x0101,robot_ID,color_ID,2,1,0,2,0,0,0,2,905,1005,0,540,540);
		ui_draw(0x0101,robot_ID,color_ID,3,1,0,2,0,0,0,2,910,1000,0,500,500);
    /*  数据内容 0x0101 绘制一个图形  0x0110绘制字符
    发送方 
    接收方 
    图形名   应该是自定义数字名称，作为标记
    操作     0空 1增加 2修改 3删除
    图形类型  0直线 1矩形  5浮点 6整型 7字符
    图层数    0-9
    颜色      0-8
    起始角    0-360
    终止角    0-360
    线宽
    起点X
    起点Y
    大小/半径
    终点X
    终点Y
*/
    osDelay(10);
    
  }
  /* USER CODE END StartTask04 */
}

/* USER CODE BEGIN Header_StartTask05 */
/**
* @brief Function implementing the myTask05 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask05 */
void StartTask05(void const * argument)
{
  /* USER CODE BEGIN StartTask05 */
  /* Infinite loop */                                                              //裁判系统数据接收线程1***************************************************************************
  for(;;)
  {
    fifo_s_init(&referee_fifo, referee_fifo_buf, REFEREE_FIFO_BUF_LENGTH);
    usart3_init(usart3_buf[0], usart3_buf[1], USART_RX_BUF_LENGHT);
		 while(1)
    {
       referee_unpack_fifo_data();//裁判系统返回参数线程
       osDelay(10);
			
    }
  }
  /* USER CODE END StartTask05 */
}

/* USER CODE BEGIN Header_StartTask06 */
/**
* @brief Function implementing the myTask06 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask06 */
void StartTask06(void const * argument)
{
  /* USER CODE BEGIN StartTask06 */
  /* Infinite loop */                                                               //裁判系统数据接收线程2***************************************************************************
  for(;;)
  {
    fifo_s_init(&referee_fifo, referee_fifo_buf, REFEREE_FIFO_BUF_LENGTH);
    usart3_init(usart3_buf[0], usart3_buf[1], USART_RX_BUF_LENGHT);
		 while(1)
    {
       referee_unpack_fifo_data();//裁判系统返回参数线程
       osDelay(10);
			
    }
  }
  /* USER CODE END StartTask06 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
