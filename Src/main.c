/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "can.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bsp_can.h"
#include "bsp_key.h"
#include "pid.h"
#include "remote.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
float ch_CAN1[7];     //can1速度
float ch_CAN2[7];     //can2速度
pid_struct_t motor_pid_CAN1[7];
pid_struct_t motor_pid_CAN2[7];
unsigned char  RX6_BUFFER[10];
unsigned char  RX8_BUFFER[10];
uint8_t buffer[1024];//存放裁判系统数据
uint8_t ctrl_mode=lose_ctrl;
uint16_t remled_cnt;
uint16_t Eye_pwmval=0;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
 
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CAN1_Init();
  MX_USART1_UART_Init();
  MX_USART6_UART_Init();
  MX_TIM7_Init();
  MX_CAN2_Init();
  MX_USART3_UART_Init();
  MX_UART8_Init();
  MX_TIM4_Init();
  MX_UART7_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOH, POWER_1_Pin|POWER_2_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOH, POWER_3_Pin|POWER_4_Pin, GPIO_PIN_RESET);
  can_filter_init();
  
  HAL_UART_Receive_IT_IDLE(&huart1,UART_Buffer,100);   //启动串口1接收
  
  __HAL_UART_ENABLE_IT(&huart8,UART_IT_IDLE);
  HAL_UART_Receive_DMA(&huart8,RX8_BUFFER,9);
  __HAL_UART_ENABLE_IT(&huart6,UART_IT_IDLE);
  HAL_UART_Receive_DMA(&huart6,RX6_BUFFER,9);
  //接收裁判系统数据
	HAL_UART_Receive_DMA(&huart3,buffer,16);
  __HAL_UART_ENABLE_IT(&huart3,UART_IT_IDLE);//开启串口空闲中断
  
  
	for (uint8_t i = 0; i < 4; i++){                                        //(PID_ID mode,pid_struct_t *pid,float kp,float ki,float kd,float i_max,float out_max,float DeadBand)
    pid_init(PID_Speed,&motor_pid_CAN1[i], 10,0.1,0, 15000, 16384,0); //init pid parameter, kp=40, ki=3, kd=0, output limit = 30000   16384
  }    
	for (uint8_t i = 4; i < 6; i++){
    pid_init(PID_Speed,&motor_pid_CAN1[i],6,0.1f, 10, 8000, 16384,0); //init pid parameter, kp=40, ki=3, kd=0, output limit = 30000   16384
  }
  for (uint8_t i = 0; i < 2; i++){
    pid_init(PID_Speed,&motor_pid_CAN2[i], 4,0.1f,15, 15000, 16384,0); //init pid parameter, kp=40, ki=3, kd=0, output limit = 30000   16384
  }
	for (uint8_t i = 2; i < 4; i++){
    pid_init(PID_Speed,&motor_pid_CAN2[i], 10,0.1,5, 15000, 16384,0); //init pid parameter, kp=40, ki=3, kd=0, output limit = 30000   16384
  }
  for (uint8_t i = 4; i < 6; i++){
    pid_init(PID_Speed,&motor_pid_CAN2[i],6,0.001f, 10, 8000, 16384,0); //init pid parameter, kp=40, ki=3, kd=0, output limit = 30000   16384
  }
  
  state_check();
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_3);
	HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin,GPIO_PIN_RESET);
	HAL_Delay(500);
  
	HAL_TIM_Base_Start_IT(&htim7);
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    HAL_Delay(1);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
